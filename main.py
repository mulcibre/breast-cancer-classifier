import numpy as np
from sklearn.neighbors import KNeighborsClassifier

#
# Load Dataset
###

# encoder for diagnosis
# diagnosis - char
# returns - float
def convertFromDiagnosisToF(diagnosis):
    if diagnosis is 'M':
        return 1.0
    elif diagnosis is 'B':
        return 0.0
    else:
        print("invalid data entry")

with open("data.txt", "r") as file:
    # all input data must be float type. encode strings as floats using converter functions
    data = np.loadtxt(file, delimiter=',', encoding='UTF-8',
                      converters={1: convertFromDiagnosisToF})

#
# Set up Identifier, Input Vars, and Target
###

# Break up data into identifiers, targets, and input variables
ids = data[:, 0]
targets = data[:, 1]
inputVars = data[:, 2:]

# Set up training/evaluation sets (generally a 70/30 split is used)
offset = int(len(ids) * 0.7)
idsTrain, targetsTrain, inputVarsTrain = ids[:offset], targets[:offset], inputVars[:offset]
idsTest, targetsTest, inputVarsTest = ids[offset:], targets[offset:], inputVars[offset:]

#
# Train Model
###

neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(inputVarsTrain, targetsTrain)

#
# Create Predictions
###

results = neigh.predict(inputVarsTest)

#
# Evaluate Model Performance
###

accuracy = neigh.score(inputVarsTest, targetsTest)

print(f"Accuracy for K nearest neigbors: {accuracy}")