# Getting started
1. [Get Pipenv](https://docs.pipenv.org/#install-pipenv-today)

2. Download repo

3. Run `pipenv install`

4. Set python version `pipenv --version 3.6.1`

5. *If using PyCharm* set project intepreter to project virtualEnv [SEE HERE](https://stackoverflow.com/questions/46251411/how-do-i-properly-setup-pipenv-in-pycharm)
